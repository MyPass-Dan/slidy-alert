/**
 * ---------------------------------------\
 *             SLIDY ALERT                 \
 * -----------------------------------------\
 * @version 1.0.8
 * @author MyPass-Dan, MyPass-Mahdi
 * 
 * Simple and minimal slide alert to feedback the user on your page. 
 * Remember to have all the dependencies required
 * for slidy alert to work properly.
 * 
 * You don't have to import FontAwesome separately as Bootstrap already has
 * that imported within it.
 * 
 * 
 * 
 * 
 * _______________________________________________
 * let args = {
 *      title: "Done",
 *      content: "Your task has been completed!",
 *      small: "fulfilled in 12.54 seconds!",
 *      type: "success"
 * };
 * 
 * slideAlert(
 *      args
 * );
 * _______________________________________________
 * 
 * 
 * 
 * 
 * ---------------------------------------\
 *             DEPENDENCIES                \
 * -----------------------------------------\
 * - Font Awesome 4.7+
 * - Bootstrap 4+ 
 * - JQuery 3.0+
 */

const icons = {
    'danger':   'exclamation',
    'warning':  'bell ',
    'primary':  'info-circle',
    'success':  'check'
};

const positions = {
    'left': [
        'align-self-start',
        'ste',
        ''
    ],
    'right': [
        'align-self-end',
        'ets',
        '-right'
    ]
}

/** 
 * parentCheck ()
 * ___________________________
 * Check for main container for slidy alert, in case
 * it cannot find it it will append it to the body
 * 
 * @author MyPass-Dan
 * @return void
 */
const parentCheck = () => {
    if(!$('#slidy-alert-fixed-container').length) $('body').prepend(`<div class="w-100 pe-none h-100 position-fixed p-3 d-flex z-1000 flex-column-reverse" id="slidy-alert-fixed-container"></div>`);
}

/**
 * slideAlert ()
 * ___________________________
 * Push a disposable alert that will destroy itself
 * after a given amount of time.
 * 
 * Note that these type of alerts are not meant to store
 * significant information, therefore you should be using them
 * for user-friendly feedback purposes only.
 * 
 * @param {Array} data 
 * 
 * @author MyPass-Dan
 * @return void
 */
 const slideAlert = (
    data,
 ) => {
    try {
        parentCheck();
    
        let container = $('#slidy-alert-fixed-container');   
    
        let title    = (data.hasOwnProperty('title')) ? data.title : 'Your Title';
        let content  = (data.hasOwnProperty('content')) ? data.content : 'Your Content';
        let small    = (data.hasOwnProperty('small')) ? data.small : '';
        let type     = (data.hasOwnProperty('type')) ? data.type : 'primary';
        let icon     = (icons.hasOwnProperty(type)) ? icons[type] : 'warning';
        let pos      = ((data.hasOwnProperty('position')) ? ((positions.hasOwnProperty(data.position)) ? positions[data.position]
                                                                                                       : positions['left'])
                                                          : positions['left']) 
                       ?? positions['left']
        ;
    
        let html = $(`
            <div class="slidy-alert${pos[2]} ${pos[0]} fade-in slide-${pos[1]}-ani slidy-alert-${type}">
                <h4 id="slidy-alert-header" class="mb-2"><span class="fa fa-${icon}"></span> ${title}</h4>
                <p id="slidy-alert-content" class="mb-0 touchable"> ${content}</p>
                <small id="slidy-alert-small" class="text-muted touchable ` + ((small == '') ? "d-none" : "")  + `"> ${small} </small>
            </div>
        `);
        container
            .append(html);
        html
            .delay(4000)
            .fadeOut('slow', function(){ 
                $(this).remove() 
            }) ;
        console.log(data.title, data.content);
    } catch (error) {
        console.error('SlidyAlert Exception:', error.message);
    }
 }